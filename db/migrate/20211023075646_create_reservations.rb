class CreateReservations < ActiveRecord::Migration[5.2]
  def change
    create_table :reservations do |t|
      t.string :code, null: false
      t.date :start_date
      t.date :end_date
      t.integer :nights
      t.integer :guests
      t.integer :adults
      t.integer :children
      t.integer :infants
      t.string :status
      t.string :currency
      t.decimal :payout_price, precision: 8, scale: 2 
      t.decimal :security_price, precision: 8, scale: 2 
      t.decimal :total_price, precision: 8, scale: 2
      t.references :guest, foreign_key: true
    end
    add_index :reservations, :code, unique: true
  end
end