class CreateGuests < ActiveRecord::Migration[5.2]
  def change
    create_table :guests do |t|
      t.string :email, null: false
      t.string :first_name
      t.string :last_name
      t.string :phone, array: true
      t.timestamps null: false
    end
    add_index :guests, :email, unique: true
    add_index :guests, :phone, using: 'gin'
  end
end