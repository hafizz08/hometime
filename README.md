# Hometime Ruby on Rails Engineer Skill Assessment

## Instructions

1. bundle install
2. modify config/database.yml and put your own db credentials
3. rake db:create:all
4. rake db:migrate
5. API Endpoint - POST /reservations - Create/Update reservations