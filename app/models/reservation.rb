class Reservation < ApplicationRecord
	belongs_to :guest

	validates_presence_of :code, :start_date, :end_date, :nights, :guests, :status, :currency, :payout_price,
						  :security_price, :total_price

	validates :code, uniqueness: true
end