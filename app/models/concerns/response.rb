module Response
  def json_response(object, nested_object = nil, status = :ok)
    if nested_object
      render json: object, include: nested_object, status: status
    else
      render json: object, status: status
    end
  end
end