class Guest < ApplicationRecord
	has_many :reservations, dependent: :destroy

	accepts_nested_attributes_for :reservations, allow_destroy: true, reject_if: :all_blank

	validates_presence_of :email, :first_name, :last_name, :phone
	validates :email, uniqueness: true
end