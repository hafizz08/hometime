class ReservationsController < ApplicationController
  before_action :alter_params, only: :create

  # POST /reservations
  def create
    @guest = Guest.find_or_initialize_by(email: guest_params[:email]) 
    @reservation = @guest.reservations.find_or_initialize_by(code: reservation_params[:code])

    # If a new record proceed to create. New record will be created if guest email and reservation code not yet exists
    if @guest.new_record? && @reservation.new_record?
      @guest_reservation = Guest.new(
        guest_params.merge(
          phone: guest_params[:phone_num].split(",")
        ).except(:phone_num)
      )
      @guest_reservation.reservations.build(reservation_params)
      @guest_reservation.save!
      json_response(@guest_reservation, [:reservations], :created)
    else 
      # If guest email exists it will go here. If there's any changes on the other attributes it will gets updated
      # If guest email exists but the reservation code not yet exists. It will create a new reservation under the same guest 
      if @reservation.new_record?
        reservation_params.to_h.each {|k,v| @reservation[k.to_sym] = v}
        @guest.update!(
          guest_params.merge(
            phone: guest_params[:phone_num].split(",")
          ).except(:phone_num)
        )
      else # If reservation code exists it will go here. If there's any changes on the other attributes it will gets updated
        reservation = reservation_params.to_h
        reservation["id"] = @reservation.id 
        @guest.update!(
          guest_params.merge(
            phone: guest_params[:phone_num].split(","),
            reservations_attributes: [reservation]
          ).except(:phone_num)
        )
      end

      json_response(@guest, [:reservations], :created)
    end
  end

  private

  def alter_params
    params[:code] = params[:reservation_code] || params[:reservation][:code]
    params[:start_date] = params[:start_date] || params[:reservation][:start_date]
    params[:end_date] = params[:end_date] || params[:reservation][:end_date]
    params[:nights] = params[:nights] || params[:reservation][:nights]
    params[:guests] = params[:guests] || params[:reservation][:number_of_guests]
    params[:adults] = params[:adults] || params[:reservation][:guest_details][:number_of_adults]
    params[:children] = params[:children] || params[:reservation][:guest_details][:number_of_children]
    params[:infants] = params[:infants] || params[:reservation][:guest_details][:number_of_infants]
    params[:status] = params[:status] || params[:reservation][:status_type]
    params[:currency] = params[:currency] || params[:reservation][:host_currency]
    params[:payout_price] = params[:payout_price] || params[:reservation][:expected_payout_amount]
    params[:security_price] = params[:security_price] || params[:reservation][:listing_security_price_accurate]
    params[:total_price] = params[:total_price] || params[:reservation][:total_paid_amount_accurate]
    params[:email] = params.try(:[], 'guest').try(:[], 'email') || params[:reservation][:guest_email]
    params[:first_name] = params.try(:[], 'guest').try(:[], 'first_name') || params[:reservation][:guest_first_name]
    params[:last_name] = params.try(:[], 'guest').try(:[], 'last_name') || params[:reservation][:guest_last_name]
    params[:phone_num] = params.try(:[], 'guest').try(:[], 'phone') || params[:reservation][:guest_phone_numbers].join(",")
  end

  def reservation_params
    params.permit(:code, :start_date, :end_date, :nights, :guests, :adults, :children, :infants, :status,
      :currency, :payout_price, :security_price, :total_price)
  end

  def guest_params
    params.permit(:email, :first_name, :last_name, :phone_num)
  end
end