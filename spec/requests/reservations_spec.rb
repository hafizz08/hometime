require 'rails_helper'
RSpec.describe 'Reservations API', type: :request do
	describe 'POST /reservations' do
		let(:first_valid_payload) {
			{
				reservation_code: "YYY12345678",
				start_date: "2021-04-14",
				end_date: "2021-04-18",
				nights: 4,
				guests: 4,
				adults: 2,
				children: 2,
				infants: 0,
				status: "accepted",
				guest: {
					first_name: "Wayne",
					last_name: "Woodbridge",
					phone: "639123456789",
					email: "wayne_woodbridge@bnb.com"
				},
				currency: "AUD",
				payout_price: "4200.00",
				security_price: "500",
				total_price: "4700.00"
			}
		}

		let(:second_valid_payload) {
			{
				reservation: {
				    code: "XXX12345678",
				    start_date: "2021-03-12",
				    end_date: "2021-03-16",
				    expected_payout_amount: "3800.00",
				    guest_details: {
				      localized_description: "4 guests",
				      number_of_adults: 2,
				      number_of_children: 2,
				      number_of_infants: 0
				    },
				    guest_email: "wayne_woodbridge@bnb.com",
				    guest_first_name: "Wayne",
				    guest_last_name: "Woodbridge",
				    guest_phone_numbers: [
				      "639123456789",
				      "639123456789"
				    ],
				    listing_security_price_accurate: "500.00",
				    host_currency: "AUD",
				    nights: 4,
				    number_of_guests: 4,
				    status_type: "accepted",
				    total_paid_amount_accurate: "4300.00"
				}
			}
		}

		let(:first_invalid_payload) {
			{
				reservation_code: "",
				start_date: "",
				end_date: "",
				nights: 4,
				guests: 4,
				adults: 2,
				children: 2,
				infants: 0,
				status: "accepted",
				guest: {
					first_name: "Wayne",
					last_name: "Woodbridge",
					phone: "639123456789",
					email: ""
				},
				currency: "",
				payout_price: "4200.00",
				security_price: "500",
				total_price: "4700.00"
			}
		}

		let(:second_invalid_payload) {
			{
				reservation: {
				    code: "",
				    start_date: "",
				    end_date: "2021-03-16",
				    expected_payout_amount: "",
				    guest_details: {
				      localized_description: "4 guests",
				      number_of_adults: 2,
				      number_of_children: 2,
				      number_of_infants: 0
				    },
				    guest_email: "",
				    guest_first_name: "Wayne",
				    guest_last_name: "Woodbridge",
				    guest_phone_numbers: [
				      "639123456789",
				      "639123456789"
				    ],
				    listing_security_price_accurate: "",
				    host_currency: "AUD",
				    nights: 4,
				    number_of_guests: 4,
				    status_type: "accepted",
				    total_paid_amount_accurate: ""
				}
			}
		}
		context 'when the request is valid for Payload #1' do
		  before { post '/reservations', params: first_valid_payload }

		  it 'creates a reservation' do
		    expect(response).to have_http_status(201)
		    expect(response.body).to include_json(
	          "id": 1,
	          "email": "wayne_woodbridge@bnb.com",
	          "first_name": "Wayne",
	          "last_name": "Woodbridge",
	          "phone": [
	              "639123456789"
	          ],
	          "reservations": [
	              {
	                  "id": 1,
	                  "code": "YYY12345678",
	                  "start_date": "2021-04-14",
	                  "end_date": "2021-04-18",
	                  "nights": 4,
	                  "guests": 4,
	                  "adults": 2,
	                  "children": 2,
	                  "infants": 0,
	                  "status": "accepted",
	                  "currency": "AUD",
	                  "payout_price": "4200.0",
	                  "security_price": "500.0",
	                  "total_price": "4700.0",
	                  "guest_id": 1
	              }
	          ]
		    )
		  end
		end

		context 'when the request is valid for Payload #2' do
		  before { post '/reservations', params: second_valid_payload }

		  it 'creates a reservation' do
		    expect(response).to have_http_status(201)
		    expect(response.body).to include_json(
	          "id": 2,
	          "email": "wayne_woodbridge@bnb.com",
	          "first_name": "Wayne",
	          "last_name": "Woodbridge",
	          "phone": [
	              "639123456789",
	              "639123456789"
	          ],
	          "reservations": [
	              {
	                  "id": 2,
	                  "code": "XXX12345678",
	                  "start_date": "2021-03-12",
	                  "end_date": "2021-03-16",
	                  "nights": 4,
	                  "guests": 4,
	                  "adults": 2,
	                  "children": 2,
	                  "infants": 0,
	                  "status": "accepted",
	                  "currency": "AUD",
	                  "payout_price": "3800.0",
	                  "security_price": "500.0",
	                  "total_price": "4300.0",
	                  "guest_id": 2
	              }
	          ]
		    )
		  end
		end

		context 'when the request is invalid for Payload #1' do
		  before { post '/reservations', params: first_invalid_payload }

		  it 'failes to create a reservation' do
		    expect(JSON.parse(response.body)['message']).to include("Validation failed") 
		  end
		end

		context 'when the request is invalid for Payload #2' do
		  before { post '/reservations', params: second_invalid_payload }

		  it 'failes to create a reservation' do
		    expect(JSON.parse(response.body)['message']).to include("Validation failed") 
		  end
		end
	end
end